#
# Be sure to run `pod lib lint opencv-contrib.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'opencv-contrib'
  s.version          = '0.0.3'
  s.summary          = 'c++ opencv2 framework for ios'

  s.description      = <<-DESC
  This pod serves as just includer of c++ opencv2 library, but it doesn't contain any swift or obj-c code.
                       DESC

  s.homepage         = 'https://gitlab.com/martingeorgiu/opencv-contrib-ios'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'martingeorgiu' => 'martin.georgiu@gmail.com' }
  s.source           = { :git => 'https://gitlab.com/martingeorgiu/opencv-contrib-ios', :tag => s.version.to_s }

  s.ios.deployment_target = '11.0'
  s.swift_version = '4.0'

  s.preserve_paths = 'opencv2.xcframework'
  s.vendored_frameworks = 'opencv2.xcframework'
  s.library = 'c++'
  s.xcconfig = { 'OTHER_LDFLAGS' => '-framework opencv2' }

  s.source_files = '**/*{.h,.hpp}'
  s.public_header_files = '**/*{.h,.hpp}'
  # s.header_dir = 'opencv2'
  # s.header_mappings_dir = 'opencv2.framework/Headers/'

end
